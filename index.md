---
title: Uppsala Big Data Meetup
---

This is a, for now, simple placeholder page for the [Uppsala Big Data Meetup](https://www.meetup.com/Uppsala-Big-Data-Meetup/)!

## Recent and Upcoming Events

* [Introduction to git](https://www.meetup.com/Uppsala-Big-Data-Meetup/events/265981530/) ([notes](git-1-notes.html))
* [Collaborating with git and simple websites with Pandoc](https://www.meetup.com/Uppsala-Big-Data-Meetup/events/266589514/)

## Some of our members

* Tilo Wiklund
